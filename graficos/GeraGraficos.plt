set term postscript eps enhanced color 
set output "custo.eps"
set datafile separator ";"
set xlabel "Número de blocos"
set ylabel "custo" 
set title "Custo x Número de blocos"
set logscale y 2
plot "BrFS.dat" using 2:3 title "BrFS" with lines,\
     "DFS.dat" using 2:3 title "DFS" with lines,\
     "UCS.dat" using 2:3 title "UCS" with lines,\
     "IDS.dat" using 2:3 title "IDS" with lines,\
     "A*.dat" using 2:3 title "A*" with lines,\
	 "BFHS.dat" using 2:3 title "BFHS" with lines,\
     "IDA*.dat" using 2:3 title "IDA*" with lines

set term postscript eps enhanced color
set output "profundidade.eps"
set datafile separator ";"
set ylabel "profundidade" 
set xlabel "Número de blocos"
set title "Profundidade x Número de blocos"
set logscale y 2
plot "BrFS.dat" using 2:4 title "BrFS" with lines,\
     "DFS.dat" using 2:4 title "DFS" with lines,\
     "UCS.dat" using 2:4 title "UCS" with lines,\
     "IDS.dat" using 2:4 title "IDS" with lines,\
     "A*.dat" using 2:4 title "A*" with lines,\
     "BFHS.dat" using 2:4 title "BFHS" with lines,\
     "IDA*.dat" using 2:4 title "IDA*" with lines

set term postscript eps enhanced color
set output "tamanho.eps"
set datafile separator ";"
set ylabel "tamanho" 
set xlabel "Número de blocos"
set title "Tamanho x Número de blocos"
set logscale y 2
plot "BrFS.dat" using 2:5 title "BrFS" with lines,\
     "DFS.dat" using 2:5 title "DFS" with lines,\
     "UCS.dat" using 2:5 title "UCS" with lines,\
     "IDS.dat" using 2:5 title "IDS" with lines,\
     "A*.dat" using 2:5 title "A*" with lines,\
	 "BFHS.dat" using 2:5 title "BFHS" with lines,\
     "IDA*.dat" using 2:5 title "IDA*" with lines

set term postscript eps enhanced color
set output "visitados.eps"
set datafile separator ";"
set ylabel "nós visitados" 
set xlabel "Número de blocos"
set title "Nós visitados x Número de blocos"
set logscale y 2
plot "BrFS.dat" using 2:6 title "BrFS" with lines,\
     "DFS.dat" using 2:6 title "DFS" with lines,\
     "UCS.dat" using 2:6 title "UCS" with lines,\
     "IDS.dat" using 2:6 title "IDS" with lines,\
     "A*.dat" using 2:6 title "A*" with lines,\
     "BFHS.dat" using 2:6 title "BFHS" with lines,\
     "IDA*.dat" using 2:6 title "IDA*" with lines

set term postscript eps enhanced color
set output "gerados.eps"
set datafile separator ";"
set ylabel "nós gerados" 
set xlabel "Número de blocos"
set title "Nós gerados x Numero de blocos"
set logscale y 2
plot "BrFS.dat" using 2:7 title "BrFS" with lines,\
     "DFS.dat" using 2:7 title "DFS" with lines,\
     "UCS.dat" using 2:7 title "UCS" with lines,\
     "IDS.dat" using 2:7 title "IDS" with lines,\
     "A*.dat" using 2:7 title "A*" with lines,\
     "BFHS.dat" using 2:7 title "BFHS" with lines,\
     "IDA*.dat" using 2:7 title "IDA*" with lines

set term postscript eps enhanced color
set output "fatorramificacao.eps"
set datafile separator ";"
set ylabel "Fator de ramificacao" 
set xlabel "Número de blocos"
set title "Fator de ramificacao x Número de blocos"
set logscale y 2
plot "BrFS.dat" using 2:8 title "BrFS" with lines,\
     "DFS.dat" using 2:8 title "DFS" with lines,\
     "UCS.dat" using 2:8 title "UCS" with lines,\
     "IDS.dat" using 2:8 title "IDS" with lines,\
     "A*.dat" using 2:8 title "A*" with lines,\
     "BFHS.dat" using 2:8 title "BFHS" with lines,\
     "IDA*.dat" using 2:8 title "IDA*" with lines

set term postscript eps enhanced color
set output "tempo.eps"
set datafile separator ";"
set ylabel "Tempo de execução" 
set xlabel "Número de blocos"
set title "Tempo de execução x Numero de blocos"
set logscale y 2
plot "BrFS.dat" using 2:9 title "BrFS" with lines,\
     "DFS.dat" using 2:9 title "DFS" with lines,\
     "UCS.dat" using 2:9 title "UCS" with lines,\
     "IDS.dat" using 2:9 title "IDS" with lines,\
     "A*.dat" using 2:9 title "A*" with lines,\
     "BFHS.dat" using 2:9 title "BFHS" with lines,\
     "IDA*.dat" using 2:9 title "IDA*" with lines

set term postscript eps enhanced color
set output "compAeIDA.eps"
set datafile separator ";"
set ylabel "Fator de ramificação" 
set xlabel "Número de blocos"
set title "Fator de ramificação x Numero de blocos"
set logscale y 2
plot "A*.dat" using 2:8 title "A*" with lines,\
	 "BFHS.dat" using 2:8 title "BFHS" with lines,\
     "IDA*.dat" using 2:8 title "IDA*" with lines

set term postscript eps enhanced color
set output "compAeIDAGerados.eps"
set datafile separator ";"
set ylabel "Nós visitados"
set xlabel "Número de blocos"
set title "Nos visitados x numero de blocos"
set logscale y 2
plot "A*.dat" using 2:6 title "A*" with lines,\
	 "BFHS.dat" using 2:6 title "BFHS" with lines,\
     "IDA*.dat" using 2:6 title "IDA*" with lines

set term postscript eps enhanced color
set output "memory.eps"
set datafile separator ";"
set ylabel "Nós em memória" 
set xlabel "Número de blocos"
set title "Nós em memória x Número de blocos"
set logscale y 2
plot "BrFS.dat" using 2:10 title "BrFS" with lines,\
     "DFS.dat" using 2:10 title "DFS" with lines,\
     "UCS.dat" using 2:10 title "UCS" with lines,\
     "IDS.dat" using 2:10 title "IDS" with lines,\
     "A*.dat" using 2:10 title "A*" with lines,\
     "BFHS.dat" using 2:10 title "BFHS" with lines,\
     "IDA*.dat" using 2:10 title "IDA*" with lines

set term postscript eps enhanced color
set output "memory.eps"
set datafile separator ";"
set ylabel "Nós em memória" 
set xlabel "Nós gerados"
set title "Nós em memória x Nós gerados"
plot "BrFS.dat" using 7:10 title "BrFS" with lines,\
     "DFS.dat" using 7:10 title "DFS" with lines,\
     "UCS.dat" using 7:10 title "UCS" with lines,\
     "IDS.dat" using 7:10 title "IDS" with lines,\
     "A*.dat" using 7:10 title "A*" with lines,\
     "BFHS.dat" using 7:10 title "BFHS" with lines,\
     "IDA*.dat" using 7:10 title "IDA*" with lines

