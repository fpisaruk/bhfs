package estrategias;

import util.Buscador;
import util.No;
import util.Resposta;

public interface EstrategiaDeBusca {
	public Resposta alcancarMeta(No noOrigem, Buscador buscador);

	public String getNome();
}