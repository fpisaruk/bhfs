package estrategias;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import main.Search;
import util.Buscador;
import util.ComparadorDeNoPorCusto;
import util.No;
import util.Resposta;

public class UCS implements EstrategiaDeBusca {

	public Resposta alcancarMeta(No noOrigem, Buscador buscador) {
		HashSet<No> nosAnalisados = new HashSet<No>();
		HashSet<No> nosNaoAnalisadosAux = new HashSet<No>();
		TreeSet<No> nosNaoAnalisados = new TreeSet<No>(
				new ComparadorDeNoPorCusto());
		nosNaoAnalisados.add(noOrigem);
		nosNaoAnalisadosAux.add(noOrigem);
		do {
			No noEmAnalise = (No) nosNaoAnalisados.first();
			nosNaoAnalisados.remove(noEmAnalise);
			nosNaoAnalisadosAux.remove(noEmAnalise);
			if (buscador.isEstadoMeta(noEmAnalise))
				return new Resposta(nosAnalisados.size()
						+ nosNaoAnalisados.size(), nosAnalisados.size(),
						nosAnalisados.size() + nosNaoAnalisados.size(),
						noEmAnalise, this);
			if (Search.debugEnabled)
				System.out.println("(" + noEmAnalise + ")");
			nosAnalisados.add(noEmAnalise);
			Vector<No> sucessores = buscador.getSucessores(noEmAnalise);
			if (Search.debugEnabled)
				System.out.println(sucessores);
			sucessores.removeAll(nosAnalisados);
			for (Iterator<No> iter = sucessores.iterator(); iter.hasNext();) {
				No noC = iter.next();
				if (!nosNaoAnalisadosAux.contains(noC))
					nosNaoAnalisados.add(noC);
			}
			nosNaoAnalisadosAux.addAll(sucessores);
			//nosNaoAnalisados.addAll(sucessores);
		} while (!nosNaoAnalisados.isEmpty());
		return null;
	}

	public String getNome() {
		return "UCS";
	}
}
