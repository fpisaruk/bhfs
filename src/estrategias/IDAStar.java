package estrategias;

import heuristicas.Heuristica;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import main.Search;
import util.Buscador;
import util.No;
import util.Resposta;

public class IDAStar implements EstrategiaDeBusca {
	private Heuristica heuristica;

	int novoCustoMaximo = -1;

	public IDAStar(Heuristica heuristica) {
		this.heuristica = heuristica;
	}

	public IDAStar(Heuristica heuristica, int custoMaximo) {
		this.heuristica = heuristica;
		novoCustoMaximo = custoMaximo;
	}

	public Resposta alcancarMeta(No noOrigem, Buscador buscador) {
		if (novoCustoMaximo == -1)
			novoCustoMaximo = heuristica.valor(noOrigem);
		int nosGerados = 0;
		int nosExpandidos = 0;
		int peakMemoryUsage = 0;
		while (true) {
			int custoMaximo = novoCustoMaximo;
			novoCustoMaximo = Integer.MAX_VALUE;
			HashMap<No, No> nosAnalisados = new HashMap<No, No>();
			LinkedList<No> nosNaoAnalisados = new LinkedList<No>();
			nosNaoAnalisados.addLast(noOrigem);
			if (Search.debugEnabled)
				System.out.println("Custo Maximo = " + custoMaximo);
			do {
				if (nosAnalisados.size() + nosNaoAnalisados.size() > peakMemoryUsage)
					peakMemoryUsage = nosAnalisados.size()
							+ nosNaoAnalisados.size();
				No noEmAnalise = (No) nosNaoAnalisados.removeLast();
				if (Search.debugEnabled)
					System.out.println("\t\t(" + noEmAnalise + ")");
				int custo = noEmAnalise.getCustoTotal(heuristica);
				if (custo <= custoMaximo) {
					nosAnalisados.put(noEmAnalise, noEmAnalise);
					if (buscador.isEstadoMeta(noEmAnalise))
						return new Resposta(peakMemoryUsage, nosExpandidos,
								nosGerados, noEmAnalise, this);
					nosExpandidos++;
					Vector<No> sucessores = buscador.getSucessores(noEmAnalise);
					sucessores.removeAll(nosNaoAnalisados);
					for (Iterator iter = sucessores.iterator(); iter.hasNext();) {
						No no = (No) iter.next();
						if (nosAnalisados.containsKey(no)) {
							No noJaAnalisado = (No) nosAnalisados.get(no);
							if (noJaAnalisado.getCusto() > no.getCusto()) {
								nosAnalisados.remove(noJaAnalisado);
								nosNaoAnalisados.addLast(no);
							}
						} else
							nosNaoAnalisados.addLast(no);
					}
					if (Search.debugEnabled)
						System.out.println(sucessores);
					nosGerados += sucessores.size();
				} else if (custo < novoCustoMaximo)
					novoCustoMaximo = custo;
			} while (!nosNaoAnalisados.isEmpty());
		}
	}

	public String getNome() {
		return "IDA*";
	}
}
