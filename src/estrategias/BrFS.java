package estrategias;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import main.Search;
import util.Buscador;
import util.No;
import util.Resposta;

public class BrFS implements EstrategiaDeBusca {
	public Resposta alcancarMeta(No noOrigem, Buscador buscador) {
		HashSet<No> nosAnalisados = new HashSet<No>();
		LinkedList<No> nosNaoAnalisados = new LinkedList<No>();
		nosNaoAnalisados.addLast(noOrigem);
		do {
			No noEmAnalise = nosNaoAnalisados.removeFirst();
			if (Search.debugEnabled)
				System.out.println(noEmAnalise);
			if (buscador.isEstadoMeta(noEmAnalise)) {
				return new Resposta(nosAnalisados.size()
						+ nosNaoAnalisados.size(), nosAnalisados.size(),
						nosAnalisados.size() + nosNaoAnalisados.size(),
						noEmAnalise, this);
			}
			nosAnalisados.add(noEmAnalise);
			Vector<No> sucessores = buscador.getSucessores(noEmAnalise);
			if (Search.debugEnabled)
				System.out.println(sucessores);
			sucessores.removeAll(nosAnalisados);
			sucessores.removeAll(nosNaoAnalisados);
			Iterator<No> i = sucessores.iterator();
			while (i.hasNext()) {
				No noAtual = i.next();
				/*if (nosNaoAnalisados.contains(noAtual)
						|| nosAnalisados.contains(noAtual))
					System.err.println("ERROOOOO");*/
				nosNaoAnalisados.addLast(noAtual);
			}
		} while (!nosNaoAnalisados.isEmpty());
		return null;
	}

	public String getNome() {
		return "BrFS";
	}
}
