package estrategias;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import main.Search;
import util.Buscador;
import util.No;
import util.Resposta;

public class DFS implements EstrategiaDeBusca {

	public Resposta alcancarMeta(No noOrigem, Buscador buscador) {
		HashMap<No, No> nosAnalisados = new HashMap<No, No>();
		LinkedList<No> nosNaoAnalisados = new LinkedList<No>();
		nosNaoAnalisados.addLast(noOrigem);
		do {
			No noEmAnalise = (No) nosNaoAnalisados.removeLast();
			if (Search.debugEnabled)
				System.out.println("(" + noEmAnalise + ")");
			if (buscador.isEstadoMeta(noEmAnalise))
				return new Resposta(nosAnalisados.size()
						+ nosNaoAnalisados.size(), nosAnalisados.size(),
						nosAnalisados.size() + nosNaoAnalisados.size(),
						noEmAnalise, this);
			nosAnalisados.put(noEmAnalise, noEmAnalise);
			Vector<No> sucessores = buscador.getSucessores(noEmAnalise);
			sucessores.removeAll(nosNaoAnalisados);
			for (Iterator iter = sucessores.iterator(); iter.hasNext();) {
				No no = (No) iter.next();
				if (nosAnalisados.containsKey(no)) {
					No noJaAnalisado = (No) nosAnalisados.get(no);
					if (noJaAnalisado.getProfundidade() > no.getProfundidade()) {
						nosAnalisados.remove(noJaAnalisado);
						nosNaoAnalisados.addLast(no);
					}
				} else
					nosNaoAnalisados.addLast(no);
			}
			if (Search.debugEnabled)
				System.out.println(sucessores);
		} while (!nosNaoAnalisados.isEmpty());
		return null;
	}

	public String getNome() {
		return "DFS";
	}

}
