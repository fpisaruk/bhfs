package estrategias;

import heuristicas.Heuristica;

import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import main.Search;
import util.Buscador;
import util.ComparadorDeNoPorCustoEHeuristica;
import util.No;
import util.Resposta;

public class AStar implements EstrategiaDeBusca {

	private Heuristica heuristica;

	public AStar(Heuristica heuristica) {
		this.heuristica = heuristica;
	}

	public Resposta alcancarMeta(No noOrigem, Buscador buscador) {
		HashSet<No> nosAnalisados = new HashSet<No>();
		HashSet<No> nosNaoAnalisadosAux = new HashSet<No>();
		TreeSet<No> nosNaoAnalisados = new TreeSet<No>(
				new ComparadorDeNoPorCustoEHeuristica(heuristica));
		nosNaoAnalisados.add(noOrigem);
		nosNaoAnalisadosAux.add(noOrigem);
		do {
			No noEmAnalise = nosNaoAnalisados.first();
			nosNaoAnalisados.remove(noEmAnalise);
			nosNaoAnalisadosAux.remove(noEmAnalise);
			if (Search.debugEnabled)
				System.out.println("(" + noEmAnalise + ")");
			if (buscador.isEstadoMeta(noEmAnalise)) {
				TreeSet<No> test = new TreeSet<No>(
						new ComparadorDeNoPorCustoEHeuristica(heuristica));
				test.addAll(nosAnalisados);
				test.addAll(nosNaoAnalisados);
				//System.out.println("TEST SIZE=" + test.size());
				//System.out.println("AUX SIZE=" + aux.size());
				return new Resposta(nosAnalisados.size()
						+ nosNaoAnalisados.size(), nosAnalisados.size(),
						nosAnalisados.size() + nosNaoAnalisados.size(),
						noEmAnalise, this);
			}
			nosAnalisados.add(noEmAnalise);
			Vector<No> sucessores = buscador.getSucessores(noEmAnalise);
			if (Search.debugEnabled)
				System.out.println(sucessores);
			sucessores.removeAll(nosAnalisados);
			for (Iterator<No> iter = sucessores.iterator(); iter.hasNext();) {
				No noC = iter.next();
				if (!nosNaoAnalisadosAux.contains(noC))
					nosNaoAnalisados.add(noC);
			}
			nosNaoAnalisadosAux.addAll(sucessores);
			// sucessores.removeAll(nosNaoAnalisados);
			//nosNaoAnalisados.addAll(sucessores);
		} while (!nosNaoAnalisados.isEmpty());
		return null;
	}

	public String getNome() {
		return "A*";
	}
}
