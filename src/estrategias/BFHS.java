package estrategias;

import heuristicas.Heuristica;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import main.Search;
import util.Buscador;
import util.ComparadorDeNoPorCustoEHeuristica;
import util.No;
import util.Resposta;

public class BFHS implements EstrategiaDeBusca {
	Buscador buscador;

	private int numNosVisitados;

	private int numNosGerados;

	private int peakMemoryUsage;

	private Heuristica heuristica;

	public BFHS(Heuristica heuristica) {
		this.heuristica = heuristica;
	}

	public Resposta alcancarMeta(No origem, Buscador buscador) {
		// System.out.println(origem);
		if (!buscador.isEstadoMeta(origem)) {
			this.buscador = buscador;
			int upperBound = heuristica.valor(origem);
			// System.out.println("DIFF=" +
			// HeuristicaDiffParaRegua.getInstance().valor(origem));
			// upperBound +=
			// ((HeuristicaDistanciaParaRegua.getInstance().valor(origem) -
			// upperBound)
			// / upperBound);
			// System.out.println("DistParaRegua="
			// + HeuristicaDistanciaParaRegua.getInstance().valor(origem));
			LinkedList<No> resposta;
			do {
				resposta = bfhs(origem, null, upperBound, 0);
				upperBound += Math.max(Math.ceil((double) upperBound / 4), 1);
			} while (resposta.size() == 0);
			int i = 0;
			for (Iterator<No> iter = resposta.iterator(); iter.hasNext();) {
				(iter.next()).setCusto(i++);
			}
			// System.out.println("RETORNOU");
			LinkedList<No> nos = resposta;
			// System.out.println("FIRST=" + nos.getFirst());
			No ant = null;
			Iterator<No> it = nos.iterator();
			while (it.hasNext()) {
				No noAtual = it.next();
				noAtual.setAntecessor(ant);
				ant = noAtual;
			}
			return new Resposta(peakMemoryUsage, numNosVisitados,
					numNosGerados, resposta.getLast(), this);
		} else
			return new Resposta(peakMemoryUsage, numNosVisitados,
					numNosGerados, origem, this);
	}

	private LinkedList<No> bfhs(No origem, No destino, int u, int r) {
		BFHSList openLists = new BFHSList();
		BFHSList closedLists = new BFHSList();
		// StringBuffer comp = new StringBuffer();
		// for (int i = 0; i < r; i++)
		// comp.append(" ");
		// System.out.println(comp + "<bfhs(" + origem + "," + destino + "," + u
		// + ")>");

		origem.setCusto(0);
		origem.setAntecessor(null);
		openLists.add(0, origem);
		int layer = 0;
		int relay = (2 * u) / 4;
		int hd = 0;
		if (destino != null)
			hd = heuristica.valor(destino);
		if (Search.debugEnabled) {
			System.out
					.println("bfhs(" + origem + "," + destino + "," + u + ")");
			System.out.println("<OpenLists>\n" + openLists + "</OpenLists>");
			System.out.println("<ClosedLists>\n" + closedLists
					+ "</ClosedLists>");
		}
		numNosGerados++;
		while (!openLists.isEmpty(layer) || !openLists.isEmpty(layer + 1)) {
			while (!(openLists.isEmpty(layer))) {
				if (openLists.getSize() + closedLists.getSize() > peakMemoryUsage)
					peakMemoryUsage = openLists.getSize()
							+ closedLists.getSize();
				if (Search.debugEnabled)
					System.out.println("Layer: " + layer);
				No noAtual = openLists.removeFirst(layer);
				numNosVisitados++;
				closedLists.add(layer, noAtual);
				if ((destino == null && buscador.isEstadoMeta(noAtual))
						|| (destino != null && destino.equals(noAtual))) {
					//System.out.println("Destino Alcancado! : " + noAtual);
					// System.out.println(new Resposta(0, 0, noAtual, this));
					openLists = null;
					/*
					 * Set relayLayer = closedLists.getList(relay); No aux =
					 * noAtual.getAntecessor(); // System.out.println(aux);
					 * while (relayLayer != null && !relayLayer.contains(aux) &&
					 * aux.getAntecessor() != null) aux = aux.getAntecessor();
					 * No middle = aux;
					 */
					No noAux = noAtual.getAntecessor();
					// System.out.println("NoAux = " + noAux);
					if (noAux.equals(origem)) {
						LinkedList<No> list = new LinkedList<No>();
						list.addFirst(origem);
						list.addLast(noAtual);
						return list;
					}
					if ((layer > 1 && layer <= relay) || (layer > relay + 1))
						deletePreviousLayer(layer, relay, origem, closedLists);
					No middle = noAtual.getAntecessor();
					if (middle.equals(origem)) {
						middle = noAux;
						noAtual.setAntecessor(middle);
						middle.setAntecessor(origem);
					}
					closedLists = null;
					// No middle = noAtual.getAntecessor();
					// System.out.println(comp + "" + comp + "Middle : " +
					// middle);
					// System.out
					// .println(comp + "" + comp + "noMeta : " + noAtual);
					// System.out.println("antecessor:" +
					// middle.getAntecessor());
					// if (middle.getAntecessor() != origem)
					// middle = middle.getAntecessor();
					LinkedList<No> left, right;
					/*
					 * if (middle.getCusto() == 0) { left = new LinkedList<No>();
					 * left.add(middle); left.add(noAtual); } else {
					 */
					if (middle.getCusto() == 1) {
						left = new LinkedList<No>();
						left.addLast(origem);
						left.addLast(middle);
					} else {
						// System.out.println("PAI Left U=" + u);
						left = bfhs(origem, middle, middle.getCusto(), r + 2);
					}
					if ((noAtual.getCusto() - middle.getCusto()) == 1) {
						right = new LinkedList<No>();
						right.addLast(middle);
						right.addLast(noAtual);
					} else
						// System.out.println("PAI Right U=" + u);
						right = bfhs(middle, noAtual, noAtual.getCusto()
								- middle.getCusto(), r + 2);

					left.removeAll(right);
					if (right.size() > 0 && left.size() > 0)
						right.getFirst().setAntecessor(left.getLast());
					left.addAll(right);
					// }
					// System.out.println(comp + "" + comp + "RESP " + left);
					// System.out.println(comp + "</bfhs(" + origem + ","
					// + destino + "," + u + ")>");
					return left;
				}
				if (Search.debugEnabled)
					System.out.println("noAtual=" + noAtual);
				expandNode(noAtual, layer, u, openLists, closedLists, hd);
				if (Search.debugEnabled) {
					System.out.println("<OpenLists>\n" + openLists
							+ "</OpenLists>");
					System.out.println("<ClosedLists>\n" + closedLists
							+ "</ClosedLists>\n");
				}
			}
			if ((layer > 1 && layer <= relay) || (layer > relay + 1))
				deletePreviousLayer(layer, relay, origem, closedLists);
			layer++;
		}
		return new LinkedList<No>();
	}

	private void deletePreviousLayer(int layer, int relay, No origem,
			BFHSList closedList) {
		Set<No> atualLayer = closedList.getList(layer);
		if (layer <= relay) {
			for (Iterator<No> iter = atualLayer.iterator(); iter.hasNext();) {
				No no = iter.next();
				no.setAntecessor(origem);
			}
		} else {
			Set<No> relayLayer = closedList.getList(relay);
			for (Iterator<No> iter = atualLayer.iterator(); iter.hasNext();) {
				No no = iter.next();
				No alfa = no.getAntecessor();
				while (!relayLayer.contains(alfa))
					alfa = alfa.getAntecessor();
				no.setAntecessor(alfa);
			}
		}
		closedList.remove(layer - 1);
		if (Search.debugEnabled)
			System.out.println("Removendo camada: " + (layer - 1));
	}

	private void expandNode(No no, int layer, int u, BFHSList openList,
			BFHSList closedList, int hd) {
		Vector<No> sucessores = buscador.getSucessores(no);
		if (Search.debugEnabled)
			System.out.println("Sucessores de " + no + " u=" + u);
		for (Iterator<No> iter = sucessores.iterator(); iter.hasNext();) {
			No sucessor = iter.next();
			// System.out.println("Analisando: " + sucessor + " dif="
			// + (heuristica.valor(sucessor) - hd));
			if ((no.getCusto() + 1 + (heuristica.valor(sucessor) - hd)) > u) {
				// System.out.println("Reprovado por custo = "
				// + (no.getCusto() + 1 + (heuristica.valor(sucessor) - hd)));
				continue;

			}
			if (closedList.contains(layer - 1, sucessor))
				continue;
			// System.out.println("Nao pertence a "
			// + closedList.getList(layer - 1));
			if (closedList.contains(layer, sucessor))
				continue;
			// System.out.println("Nao pertence a " +
			// closedList.getList(layer));
			if (openList.contains(layer + 1, sucessor))
				continue;
			// System.out.println("Nao pertence a " + openList.getList(layer +
			// 1));
			if (openList.contains(layer, sucessor))
				continue;
			// System.out.println("Nao pertence a " + openList.getList(layer));
			sucessor.setCusto(no.getCusto() + 1);
			if (Search.debugEnabled)
				System.out.println(sucessor);
			openList.add(layer + 1, sucessor);
			numNosGerados++;
		}
	}

	public String getNome() {
		return "BFHS";
	}

	private class BFHSList {
		private LinkedHashMap<Integer, LinkedHashSet<No>> myList;

		private LinkedHashMap<Integer, TreeSet<No>> myTree;

		private int size;

		public int getSize() {
			return size;
		}

		private BFHSList() {
			myList = new LinkedHashMap<Integer, LinkedHashSet<No>>();
			myTree = new LinkedHashMap<Integer, TreeSet<No>>();
			size = 0;
		}

		HashSet<No> getList(int layer) {
			return myList.get(layer);
		}

		void add(int layer, No no) {
			size++;
			if (myList.containsKey(layer)) {
				HashSet<No> list = myList.get(layer);
				TreeSet<No> tree = myTree.get(layer);
				list.add(no);
				tree.add(no);
			} else {
				LinkedHashSet<No> list = new LinkedHashSet<No>();
				TreeSet<No> tree = new TreeSet<No>(
						new ComparadorDeNoPorCustoEHeuristica(heuristica));
				list.add(no);
				tree.add(no);
				myList.put(layer, list);
				myTree.put(layer, tree);
			}
		}

		void remove(int layer, No no) {
			size--;
			if (myList.containsKey(layer)) {
				HashSet<No> list = myList.get(layer);
				list.remove(no);
				TreeSet<No> tree = myTree.get(layer);
				tree.remove(no);
				if (list.isEmpty()) {
					myList.remove(layer);
					myTree.remove(layer);
				}

			}
		}

		No removeFirst(int layer) {
			if (myList.containsKey(layer)) {
				TreeSet<No> tree = myTree.get(layer);
				No no = tree.first();
				// HashSet<No> list = getList(layer);
				size--;
				// No no = list.iterator().next();
				// list.remove(no);
				remove(layer, no);
				// if (list.isEmpty())
				// myList.remove(layer);
				return no;
			} else
				return null;
		}

		void remove(int layer) {
			if (myList.containsKey(layer)) {
				size -= myList.get(layer).size();
				myList.remove(layer);
				myTree.remove(layer);
			}
		}

		boolean isEmpty(int layer) {
			return !myList.containsKey(layer);
		}

		boolean contains(int layer, No no) {
			if (myList.containsKey(layer)) {
				HashSet<No> list = myList.get(layer);
				return list.contains(no);
			} else
				return false;
		}

		public String toString() {
			StringBuffer sb = new StringBuffer();
			TreeSet<Integer> keySet = new TreeSet<Integer>(myList.keySet());
			for (Iterator<Integer> iter = keySet.iterator(); iter.hasNext();) {
				int layer = iter.next();
				sb.append("\t<" + layer + ">");
				sb.append(getList(layer));
				sb.append("</" + layer + ">\n");
			}
			return sb.toString();
		}
	}
}
