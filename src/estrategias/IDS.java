package estrategias;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import main.Search;
import util.Buscador;
import util.No;
import util.Resposta;

public class IDS implements EstrategiaDeBusca {
	int profundidadeMaxima = -1;

	
	public IDS() {
	}
	
	public IDS(int profInicial) {
		profundidadeMaxima = profInicial;
	}

	public Resposta alcancarMeta(No noOrigem, Buscador buscador) {
		if (profundidadeMaxima == -1)
			profundidadeMaxima = 0;
		int nosGerados = 0;
		int nosExpandidos = 0;
		int peakMemoryUsage = 0;
		while (true) {
			HashMap<No, No> nosAnalisados = new HashMap<No, No>();
			LinkedList<No> nosNaoAnalisados = new LinkedList<No>();
			nosNaoAnalisados.addLast(noOrigem);
			if (Search.debugEnabled)
				System.out.println("Profundidade maxima=" + profundidadeMaxima);
			while (!nosNaoAnalisados.isEmpty()) {
				if (nosAnalisados.size() + nosNaoAnalisados.size() > peakMemoryUsage)
					peakMemoryUsage = nosAnalisados.size()
							+ nosNaoAnalisados.size();
				No noEmAnalise = (No) nosNaoAnalisados.removeLast();
				if (Search.debugEnabled)
					System.out.println("(" + noEmAnalise + ")");
				if (buscador.isEstadoMeta(noEmAnalise)) {
					nosExpandidos += nosAnalisados.size();
					nosGerados += nosNaoAnalisados.size();
					return new Resposta(peakMemoryUsage, nosExpandidos,
							nosGerados, noEmAnalise, this);
				}

				if (noEmAnalise.getProfundidade() < profundidadeMaxima) {
					nosAnalisados.put(noEmAnalise, noEmAnalise);
					Vector<No> sucessores = buscador.getSucessores(noEmAnalise);
					sucessores.removeAll(nosNaoAnalisados);
					for (Iterator iter = sucessores.iterator(); iter.hasNext();) {
						No no = (No) iter.next();
						if (nosAnalisados.containsKey(no)) {
							No noJaAnalisado = (No) nosAnalisados.get(no);
							if (noJaAnalisado.getProfundidade() > no
									.getProfundidade()) {
								nosAnalisados.remove(noJaAnalisado);
								nosNaoAnalisados.addLast(no);
							}
						} else
							nosNaoAnalisados.addLast(no);
					}
					if (Search.debugEnabled)
						System.out.println(sucessores);
				}
			}
			nosGerados += nosAnalisados.size() + nosNaoAnalisados.size();
			nosExpandidos += nosAnalisados.size();
			profundidadeMaxima++;
		}
	}

	public String getNome() {
		return "IDS";
	}

}
