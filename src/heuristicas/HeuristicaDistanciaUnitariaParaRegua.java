package heuristicas;

import util.No;
import util.Regua;

/**
 * 
 * @author pisaruk
 * 
 * Não eh admissivel para a regua de custo unitário por operacao.
 */
public class HeuristicaDistanciaUnitariaParaRegua implements Heuristica {
	private static HeuristicaDistanciaUnitariaParaRegua instance = new HeuristicaDistanciaUnitariaParaRegua();

	private HeuristicaDistanciaUnitariaParaRegua() {
	}

	public static HeuristicaDistanciaUnitariaParaRegua getInstance() {
		return instance;
	}

	public int valor(No no) {
		int valor = ((Regua) no.getObj()).getDistanciaUnitariaTrocas();
		/*
		 * if (no.getAntecessor()!=null){ int valorPai =
		 * valor(no.getAntecessor()); return Math.min(valor, valorPai); }
		 */
		return valor;
	}
}
