package heuristicas;

import util.No;
import util.Regua;


public class HeuristicaDistanciaParaRegua implements Heuristica {
	private static HeuristicaDistanciaParaRegua instance = new HeuristicaDistanciaParaRegua();

	private HeuristicaDistanciaParaRegua() {
	}

	public static HeuristicaDistanciaParaRegua getInstance() {
		return instance;
	}

	public int valor(No no) {
		int valor = ((Regua) no.getObj()).getDistanciaTrocas();
		/*
		 * if (no.getAntecessor()!=null){ int valorPai =
		 * valor(no.getAntecessor()); return Math.min(valor, valorPai); }
		 */
		return valor;
	}
}

