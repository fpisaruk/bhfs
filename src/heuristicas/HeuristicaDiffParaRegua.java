package heuristicas;

import util.No;
import util.Regua;

public class HeuristicaDiffParaRegua implements Heuristica {
	private static HeuristicaDiffParaRegua instance = new HeuristicaDiffParaRegua();

	private HeuristicaDiffParaRegua() {
	}

	public static HeuristicaDiffParaRegua getInstance() {
		return instance;
	}

	public int valor(No no) {
		return ((Regua) no.getObj()).getMinDiff(); 
	}
}
