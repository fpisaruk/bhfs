package main;

import heuristicas.HeuristicaDiffParaRegua;
import heuristicas.HeuristicaDistanciaParaRegua;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import util.BuscadorDaRegua;
import util.No;
import util.Regua;
import util.Resposta;
import estrategias.AStar;
import estrategias.BFHS;
import estrategias.BFHS_IDS;
import estrategias.BrFS;
import estrategias.DFS;
import estrategias.IDAStar;
import estrategias.IDS;
import estrategias.UCS;

/**
 * EP1 de Introducao a Inteligencia Artifical - 2006 </br>
 * 
 * nome: Fabio Pisaruk. NUSP: 3467959
 */
public class Search {
	public static int repeticoes = 0;

	public static boolean debugEnabled = false;

	private static void imprimeInfoUso() {
		System.out
				.println("Uso: java main.Main (<arquivo> <estrategia de busca>) | benchmark [-debug]");
		System.out.println("Estratégias de busca:");
		System.out.println("BrFS - Busca em largura");
		System.out.println("BFHS - Busca em largura com heurística");
		System.out
				.println("BFHS_IDS - Busca em largura com heurística usando IDS nos subproblemas");
		System.out.println("DFS - Busca em profundidade");
		System.out.println("UCS - Busca com custo uniforme");
		System.out.println("IDS - Busca em profundidade iterativa");
		System.out.println("A* - Busca A*");
		System.out.println("IDA* - Busca IDA*");
		System.out.println("Exemplos: ");
		System.out.println("java main.Main puz1 BrFS");
		System.out.println("java main.Main puz2 A*");
		System.out.println("java main.Main puz2 A* -debug");
		System.out.println("java main.Main benchmark");
		System.exit(-1);
	}

	private static void benchmark() {
		BuscadorDaRegua buscador = new BuscadorDaRegua();
		int numReguasInicial = 256;
		int numReguas = numReguasInicial;
		Vector reguasGlobal[] = new Vector[15];

		for (int i = 2; i <= 16; i++) {
			reguasGlobal[i - 2] = Regua.geraReguasAleatoriamente(i, numReguas);
			numReguas = Math.max(numReguas / 2, 2);
		}
		System.out
				.println("estratégia;numBlocos;custo;profundidade;tamanho;visitados;gerados;fatorRamificacao;tempo;peakMemory;MemoryDivGerados");

		for (int i = 2; i <= 8; i++) {
			Vector reguas = reguasGlobal[i - 2];
			Resposta respostas[] = new Resposta[reguas.size()];
			int h = 0;
			long iniTime = System.currentTimeMillis();
			for (Iterator iter = reguas.iterator(); iter.hasNext(); h++) {
				No noRaiz = new No(0, iter.next(), null);
				// System.err.println("<" + noRaiz + ">");
				respostas[h] = buscador.buscar(noRaiz, new BFHS(
						HeuristicaDiffParaRegua.getInstance()));
				// System.err.println("</" + noRaiz + ">");
			}
			imprimeResultado(i, respostas, iniTime, "BFHS.dat");
		}

		for (int i = 2; i <= 8; i++) {
			Vector reguas = reguasGlobal[i - 2];
			Resposta respostas[] = new Resposta[reguas.size()];
			int h = 0;
			long iniTime = System.currentTimeMillis();
			for (Iterator iter = reguas.iterator(); iter.hasNext(); h++) {
				No noRaiz = new No(0, iter.next(), null);
				// System.err.println("<" + noRaiz + ">");
				respostas[h] = buscador.buscar(noRaiz, new BFHS_IDS(
						HeuristicaDiffParaRegua.getInstance()));
				// System.err.println("</" + noRaiz + ">");
			}
			imprimeResultado(i, respostas, iniTime, "BFHS_IDS.dat");
		}
		
		for (int i = 2; i <= 6; i++) {
			Vector reguas = reguasGlobal[i - 2];
			Resposta respostas[] = new Resposta[reguas.size()];
			int h = 0;
			long iniTime = System.currentTimeMillis();
			for (Iterator iter = reguas.iterator(); iter.hasNext(); h++) {
				No noRaiz = new No(0, iter.next(), null);
				respostas[h] = buscador.buscar(noRaiz, new BrFS());
			}
			imprimeResultado(i, respostas, iniTime, "BrFS.dat");
		}

		for (int i = 2; i <= 7; i++) {
			Vector reguas = reguasGlobal[i - 2];
			Resposta respostas[] = new Resposta[reguas.size()];
			int h = 0;
			long iniTime = System.currentTimeMillis();
			for (Iterator iter = reguas.iterator(); iter.hasNext(); h++) {
				No noRaiz = new No(0, iter.next(), null);
				respostas[h] = buscador.buscar(noRaiz, new DFS());
			}
			imprimeResultado(i, respostas, iniTime, "DFS.dat");
		}

		for (int i = 2; i <= 7; i++) {
			Vector reguas = reguasGlobal[i - 2];
			Resposta respostas[] = new Resposta[reguas.size()];
			int h = 0;
			long iniTime = System.currentTimeMillis();

			for (Iterator iter = reguas.iterator(); iter.hasNext(); h++) {
				No noRaiz = new No(0, iter.next(), null);
				respostas[h] = buscador.buscar(noRaiz, new UCS());
			}
			imprimeResultado(i, respostas, iniTime, "UCS.dat");
		}

		for (int i = 2; i <= 7; i++) {
			Vector reguas = reguasGlobal[i - 2];
			Resposta respostas[] = new Resposta[reguas.size()];
			int h = 0;
			long iniTime = System.currentTimeMillis();
			for (Iterator iter = reguas.iterator(); iter.hasNext(); h++) {
				No noRaiz = new No(0, iter.next(), null);
				respostas[h] = buscador.buscar(noRaiz, new IDS());
			}
			imprimeResultado(i, respostas, iniTime, "IDS.dat");
		}

		for (int i = 2; i <= 8; i++) {
			Vector reguas = reguasGlobal[i - 2];
			Resposta respostas[] = new Resposta[reguas.size()];
			int h = 0;
			long iniTime = System.currentTimeMillis();
			for (Iterator iter = reguas.iterator(); iter.hasNext(); h++) {
				No noRaiz = new No(0, iter.next(), null);
				respostas[h] = buscador.buscar(noRaiz, new AStar(
						HeuristicaDistanciaParaRegua.getInstance()));
			}
			imprimeResultado(i, respostas, iniTime, "A*.dat");
		}

		for (int i = 2; i <= 8; i++) {
			Vector reguas = reguasGlobal[i - 2];
			Resposta respostas[] = new Resposta[reguas.size()];
			int h = 0;
			long iniTime = System.currentTimeMillis();
			for (Iterator iter = reguas.iterator(); iter.hasNext(); h++) {
				No noRaiz = new No(0, iter.next(), null);
				respostas[h] = buscador.buscar(noRaiz, new IDAStar(
						HeuristicaDistanciaParaRegua.getInstance()));
			}
			imprimeResultado(i, respostas, iniTime, "IDA*.dat");
		}
	}

	private static void imprimeResultado(int numDeBlocos, Resposta[] respostas,
			long iniTime, String filename) {
		FileWriter fw = null;
		try {
			fw = new FileWriter(filename, true);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		double medias[] = new double[8];
		medias[6] = (System.currentTimeMillis() - iniTime) / numDeBlocos;
		for (int h = 0; h < respostas.length; h++) {
			medias[0] += respostas[h].getCustoSolucao();
			medias[1] += respostas[h].getProfundidadeDaMeta();
			medias[2] += respostas[h].getTamanho();
			medias[3] += respostas[h].getNumNosExpandidos();
			medias[4] += respostas[h].getNumNosGerados();
			medias[7] += respostas[h].getPeakMemoryUsage();
		}
		medias[7] /= respostas.length;
		medias[5] = medias[4] / medias[3];
		for (int j = 0; j < 5; j++)
			medias[j] /= respostas.length;

		String resp = respostas[0].getEstrategiaDeBusca().getNome() + ";"
				+ numDeBlocos + ";" + medias[0] + ";" + medias[1] + ";"
				+ medias[2] + ";" + medias[3] + ";" + medias[4] + ";"
				+ medias[5] + ";" + medias[6] + ";" + medias[7] + ";"
				+ medias[7] / medias[4];
		System.out.println(resp);
		try {
			fw.append(resp + "\n");
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		if (args.length == 0)
			imprimeInfoUso();

		for (int i = 0; i < args.length; i++)
			if (args[i].equalsIgnoreCase("-debug")) {
				debugEnabled = true;
				break;
			}

		if (args[0].equalsIgnoreCase("benchmark"))
			benchmark();
		else {
			if (!((args.length == 2 && !debugEnabled) || (args.length == 3 && debugEnabled)))
				imprimeInfoUso();
			String estrategiaDeBusca = args[1];
			String nomeDoArquivo = args[0];
			Resposta resposta = null;
			No noRaiz = new No(0, new Regua(nomeDoArquivo), null);

			long iniTime = System.currentTimeMillis();
			BuscadorDaRegua buscador = new BuscadorDaRegua();
			if (estrategiaDeBusca.equalsIgnoreCase("brfs"))
				resposta = buscador.buscar(noRaiz, new estrategias.BrFS());
			else if (estrategiaDeBusca.equalsIgnoreCase("bfhs"))
				resposta = buscador.buscar(noRaiz, new BFHS(
						HeuristicaDiffParaRegua.getInstance()));
			else if (estrategiaDeBusca.equalsIgnoreCase("bfhs_ids"))
				resposta = buscador.buscar(noRaiz, new BFHS_IDS(
						HeuristicaDiffParaRegua.getInstance()));
			else if (estrategiaDeBusca.equalsIgnoreCase("dfs"))
				resposta = buscador.buscar(noRaiz, new DFS());
			else if (estrategiaDeBusca.equalsIgnoreCase("ucs"))
				resposta = buscador.buscar(noRaiz, new UCS());
			else if (estrategiaDeBusca.equalsIgnoreCase("ids"))
				resposta = buscador.buscar(noRaiz, new IDS());
			else if (estrategiaDeBusca.equalsIgnoreCase("a*"))
				resposta = buscador.buscar(noRaiz, new AStar(
						HeuristicaDistanciaParaRegua.getInstance()));
			else if (estrategiaDeBusca.equalsIgnoreCase("ida*"))
				resposta = buscador.buscar(noRaiz, new IDAStar(
						HeuristicaDistanciaParaRegua.getInstance()));
			else {
				imprimeInfoUso();
			}
			System.out.println("Solução: ");
			if (resposta == null)
				return;
			Vector caminho = resposta.getNosDaSolucao();
			for (Iterator iter = caminho.iterator(); iter.hasNext();) {
				System.out.println((No) iter.next());
			}
			System.out.println("número máximo de nós armazenados..."
					+ resposta.getPeakMemoryUsage());
			System.out.println("numero de nós visitados............"
					+ resposta.getNumNosExpandidos());
			System.out.println("numero de nós gerados.............."
					+ resposta.getNumNosGerados());
			System.out.println("profundidade da meta..............."
					+ resposta.getProfundidadeDaMeta());
			System.out.println("custo da solução..................."
					+ resposta.getCustoSolucao());
			System.out.println("fator de ramificação médio........."
					+ resposta.getFatorMedioRamificacao());
			System.out.println("tamanho da solução................."
					+ resposta.getTamanho());
			System.out.println("tempo de execução.................."
					+ (System.currentTimeMillis() - iniTime) + "ms");
			System.out.println("repetições: " + repeticoes);
			// System.out.println(1<<2);
			/*
			 * Vector<Regua> reguas = Regua.geraReguasAleatoriamente(2, 10);
			 * for (Iterator<Regua> iter = reguas.iterator(); iter.hasNext();) {
			 * Regua cur = iter.next(); cur.calculaHashCode();
			 * System.out.println(cur + " = " + cur.hashCode()); }
			 */
		}
	}
}
