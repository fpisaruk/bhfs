package util;

import java.util.LinkedList;
import java.util.Vector;

import estrategias.EstrategiaDeBusca;

public class Resposta {
	private int numNosExpandidos;

	private int numNosGerados;

	private Vector<No> nosDaSolucao;

	private int profundidadedaMeta;

	private int custoSolucao;

	private int peakMemoryUsage;

	private EstrategiaDeBusca estrategiaDeBusca;

	public Resposta(int peakMemoryUsage, int nosExpandidos, int nosGerados,
			No noMeta, EstrategiaDeBusca estrategiaDeBusca) {
		this.peakMemoryUsage = peakMemoryUsage;
		numNosExpandidos = nosExpandidos;
		numNosGerados = nosGerados;
		nosDaSolucao = noMeta.getCaminho();
		custoSolucao = noMeta.getCusto();
		profundidadedaMeta = noMeta.getProfundidade();
		this.estrategiaDeBusca = estrategiaDeBusca;
	}

	public int getNumNosExpandidos() {
		return numNosExpandidos;
	}

	public int getNumNosGerados() {
		return numNosGerados;
	}

	public int getTamanho() {
		return nosDaSolucao.size() - 1;
	}

	public int getCustoSolucao() {
		return custoSolucao;
	}

	public double getFatorMedioRamificacao() {
		return (double) numNosGerados / numNosExpandidos;
	}

	public Vector<No> getNosDaSolucao() {
		return nosDaSolucao;
	}

	public int getProfundidadeDaMeta() {
		return profundidadedaMeta;
	}

	public EstrategiaDeBusca getEstrategiaDeBusca() {
		return estrategiaDeBusca;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Resposta>");
		sb.append(getNosDaSolucao());
		sb.append("</Resposta>");
		return sb.toString();
	}

	public int getPeakMemoryUsage() {
		return peakMemoryUsage;
	}
}
