package util;

import heuristicas.Heuristica;
import heuristicas.HeuristicaDiffParaRegua;

import java.util.Vector;

public class No {
	private int custo;

	private Object obj;

	private long id;

	private static long next_id = 0;

	private No antecessor;

	private int profundidade;

	public static void reiniciarContadorDeID() {
		// TODO É necessario?
		synchronized (No.class) {
			next_id = 0;
		}
	}

	public No getAntecessor() {
		return antecessor;
	}

	public long getId() {
		return id;
	}

	public int getProfundidade() {
		return profundidade;
	}

	public No(int cost, Object obj, No antecessor) {
		this.custo = cost;
		this.obj = obj;
		this.antecessor = antecessor;
		if (antecessor == null)
			profundidade = 0;
		else
			profundidade = antecessor.profundidade + 1;
		// synchronized (No.class) {
		this.id = next_id++;
		// }
	}

	public int getCusto() {
		return custo;
	}

	public void setCusto(int custo) {
		this.custo = custo;
	}
	
	public void setProfundidade(int prof) {
		this.profundidade = prof;
	}

	public void setAntecessor(No antecessor) {
		this.antecessor = antecessor;
	}

	/**
	 * Retorna o caminho reverso deste no ao n� origem caminhando pelos
	 * antecessores.
	 * 
	 * @return Vector contendo os n�s que formam o caminho reverso do n� atual �
	 *         raiz.
	 */
	public Vector<No> getCaminho() {
		Vector<No> nos = new Vector<No>();
		No noAtual = this;
		while (noAtual.getAntecessor() != null) {
			nos.add(noAtual);
			noAtual = noAtual.getAntecessor();
		}
		nos.add(noAtual);
		for (int i = 0; i < nos.size() / 2; i++) {
			No noSwap = (No) nos.elementAt(i);
			nos.setElementAt(nos.elementAt(nos.size() - i - 1), i);
			nos.setElementAt(noSwap, nos.size() - i - 1);
		}
		return nos;

	}

	public int getCustoTotal(Heuristica heuristica) {
		return heuristica.valor(this) + custo;
	}

	public Object getObj() {
		return obj;
	}

	public boolean equals(Object outro) {
		No outroNo = (No) outro;
		return outroNo.getObj().equals(obj);
	}

	public int hashCode() {
		return obj.hashCode();
	}

	public String toString() {
		return "<" + obj.toString() + " g=" + custo + " id=" + id + " h1="
				+ HeuristicaDiffParaRegua.getInstance().valor(this) + ">";
	}
}
