package util;

import heuristicas.Heuristica;

import java.util.Comparator;

import main.Search;

public class ComparadorDeNoPorCustoEHeuristica implements Comparator<No> {
	private Heuristica heuristica;

	public ComparadorDeNoPorCustoEHeuristica(Heuristica heuristica) {
		this.heuristica = heuristica;
	}

	/**
	 * Nao esta consistente com o equals :(
	 */
	public int compare(No noA, No noB) {
		if (noA.equals(noB)) return 0;
		int fnA = noA.getCustoTotal(heuristica);
		int fnB = noB.getCustoTotal(heuristica);
		if (fnA == fnB) {
			int h2A = noA.getCusto(); // HeuristicaDiffParaRegua.getInstance().valor(noA);
			int h2B = noB.getCusto();// HeuristicaDiffParaRegua.getInstance().valor(noB);
			if (h2A == h2B) {
				//Search.repeticoes++;
				return (int)(noA.getId() - noB.getId());
				
				/*if (noA.hashCode() > noB.hashCode())
					return 1;
				else if (noA.hashCode() < noB.hashCode())
					return -1;
				else {
					//Search.repeticoes++;
					return 0;
				}
				*/
			} else
				return h2A - h2B;
		} else
			return fnA - fnB;
	}
}
