package util;

import java.util.Comparator;

public class ComparadorDeNoPorCusto implements Comparator<No> {

	public int compare(No noA, No noB) {
		int resp;

		if (noB.equals(noA))
			resp = 0;
		else {
			if (noA.getCusto() == noB.getCusto()) {
				if (noA.hashCode() > noB.hashCode())
					resp = 1;
				else if (noA.hashCode() < noB.hashCode())
					resp = -1;
				else
					resp = (int) (noA.getId() - noB.getId());
			} else
				resp = noA.getCusto() - noB.getCusto();
		}
		// System.out.println("Comparando " + noA + " com " + noB + " = " +
		// resp);
		return resp;
	}
}
