package util;

import java.util.Vector;

import estrategias.EstrategiaDeBusca;

public interface Buscador {
	public Vector<No> getSucessores(No obj);

	public boolean isEstadoMeta(No obj);

	public Resposta buscar(No noOrigem, EstrategiaDeBusca estrategia);
}
