package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

public final class Regua {
	private char regua[];

	private String strRegua;

	private int posicaoDoVazio;

	private int hashCode;

	private int distTrocas = -1;

	public static Regua deslocar(Regua regua, int deslocamento) {
		try {
			Regua nova = (Regua) regua.clone();
			nova.desloca(deslocamento);
			return nova;
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	protected Object clone() throws CloneNotSupportedException {
		return new Regua(regua);
	}

	public void calculaHashCode() {

		char c[] = new char[strRegua.length()];
		strRegua.getChars(0, strRegua.length(), c, 0);
		int resp = 0;
		for (int i = 0; i < c.length; i++)
			if (c[i] == 'A')
				resp += (1 << (30 - i));

		// hashCode = resp;
		// hashCode = resp + (1 << posicaoDoVazio);
		hashCode = strRegua.hashCode();
	}

	private Regua(char regua[]) {
		this.regua = new char[regua.length];
		for (int i = 0; i < regua.length; i++) {
			this.regua[i] = regua[i];
			if (regua[i] == '-')
				posicaoDoVazio = i;
		}
		strRegua = String.valueOf(regua);
		hashCode = strRegua.hashCode();
		calculaHashCode();
	}

	/**
	 * Controi uma Regua a partir dos dados do aruivo passado
	 * 
	 * @param fileName
	 *            Nome do arquivo
	 */
	public Regua(String fileName) {
		BufferedReader bf;
		try {
			bf = new BufferedReader(new FileReader(fileName));
			bf.readLine();
			String r = bf.readLine();
			strRegua = r;
			hashCode = strRegua.hashCode();
			calculaHashCode();
			posicaoDoVazio = r.indexOf('-');
			regua = r.toCharArray();
		} catch (Exception e) {
			throw new IllegalArgumentException(
					"Arquivo nao existente ou com formato invalido.", e);
		}
	}

	/**
	 * Constroi uma regua a partir de uma sequencia de caracteres e um tamanho
	 * 
	 * TODO validar regua
	 * 
	 * @param strRegua
	 * @param numeroDeBlocos
	 *            Por exemplo: a regua AA-BB possui 4 blocos
	 */
	public Regua(String strRegua, int numeroDeBlocos) {
		posicaoDoVazio = strRegua.indexOf('-');
		regua = strRegua.toCharArray();
		this.strRegua = strRegua;
		hashCode = strRegua.hashCode();
		calculaHashCode();
	}

	/**
	 * TODO implementar
	 * 
	 * @param regua
	 * @return
	 */
	/*
	 * private boolean reguaValida(char[] regua) { return true; }
	 */

	/**
	 * 
	 * @param n
	 */
	public void desloca(int n) {
		if (deslocamentoPossivel(n)) {
			regua[posicaoDoVazio] = regua[posicaoDoVazio + n];
			posicaoDoVazio += n;
			regua[posicaoDoVazio] = '-';
			strRegua = String.valueOf(regua);
			hashCode = strRegua.hashCode();
			calculaHashCode();
			distTrocas = -1;
		}
	}

	/**
	 * Verifica se um dado deslocamento é permitido
	 * 
	 * @param i
	 * @return
	 */
	private boolean deslocamentoPossivel(int i) {
		return (Math.abs(i) <= getNumeroDeBlocos()) && (i != 0)
				&& (posicaoDoVazio + i >= 0)
				&& (posicaoDoVazio + i < regua.length);
	}

	public String toString() {
		return strRegua;
	}

	/**
	 * Retorna verdadeiro se esta regua representa um estado meta
	 * 
	 * TODO melhorar isso considerando os dois casos da posicao do espaco
	 * 
	 * @return
	 */
	public boolean isEstadoMeta() {
		int contB = 0;
		for (int i = 0; i < regua.length && regua[i] != 'A'; i++)
			if (regua[i] == 'B')
				contB++;
		return contB == getNumeroDeBlocos();
	}

	private int getNumeroDeBlocos() {
		return (regua.length - 1) / 2;
	}

	/**
	 * Retorna o maior deslocamento permnitido para a esquerda
	 * 
	 * @return
	 */
	public int getMaxDeslocamentoParaEsquerda() {
		return Math.max(-getNumeroDeBlocos(), -posicaoDoVazio);
	}

	/**
	 * Retorna o maximo deslocamento permitido para a direita
	 * 
	 * @return
	 */
	public int getMaxDeslocamentoParaDireita() {
		return Math.min(getNumeroDeBlocos(), regua.length - posicaoDoVazio - 1);
	}

	public boolean equals(Object obj) {
		Regua outra = (Regua) obj;
		if (outra.hashCode() == hashCode())
			return outra.asString().equals(asString());
		return false;
	}

	public int hashCode() {
		return hashCode;
	}

	public String asString() {
		return strRegua;
	}

	/**
	 * Controi todas as metas possiveis para um dado tamanho de regua
	 * 
	 * @param numeroDeBlocos
	 */
	public static Vector<Regua> getMetasPossiveis(int numBlocos) {
		int size = numBlocos * 2 + 1;
		Vector<Regua> reguas = new Vector<Regua>(size);
		char regChar[] = new char[size];
		int i;
		for (i = 0; i < numBlocos; i++)
			regChar[i] = 'B';
		regChar[i++] = '-';
		for (; i < regChar.length; i++)
			regChar[i] = 'A';
		Regua reguaMetaBase = new Regua(regChar);
		for (i = reguaMetaBase.getMaxDeslocamentoParaEsquerda(); i <= reguaMetaBase
				.getMaxDeslocamentoParaDireita(); i++)
			reguas.add(Regua.deslocar(reguaMetaBase, i));
		return reguas;
	}

	/**
	 * Retorna o numero de blocos que diferem entre esta regua e a passada como
	 * parametro
	 * 
	 * @return
	 */
	private int getDiff(Regua outra) {
		char reguaChar[] = outra.asString().toCharArray();
		int numeroDePosicoesDiferentes = 0;
		for (int i = 0; i < outra.getTamanho(); i++) {
			if (reguaChar[i] != regua[i] && regua[i] != '-'&& reguaChar[i] != '-')
				numeroDePosicoesDiferentes++;
		}
		return numeroDePosicoesDiferentes;
	}

	public int getMinDiff() {
		if (isEstadoMeta()) return 0; 
		Vector respostas = getMetasPossiveis(getNumeroDeBlocos());
		int min = Integer.MAX_VALUE;
		for (Iterator iter = respostas.iterator(); iter.hasNext();) {
			int diff = ((Regua) iter.next()).getDiff(this);
			if (diff < min)
				min = diff;
		}
		return Math.max(min,1);
	}

	public int getTamanho() {
		return this.regua.length;
	}

	/**
	 * 
	 * @param regua
	 * @return
	 */
	public int getDistanciaTrocas() {
		if (distTrocas == -1)
			distTrocas = calculaDistanciaTrocas();
		return distTrocas;
	}

	/**
	 * 
	 * @param regua
	 * @return
	 */
	public int getDistanciaUnitariaTrocas() {
		return calculaDistanciaUnitariaTrocas();
	}

	private int calculaDistanciaTrocas() {
		// System.out.println("<calculaDistanciaTrocas()>");
		int custo = 0;
		int ub = strRegua.lastIndexOf('B');
		int pa = strRegua.indexOf('A');
		int ta = getNumeroDeBlocos();
		int tb = getNumeroDeBlocos();
		while (pa <= getNumeroDeBlocos() && pa >= 0) {
			// System.out.println("custo="+custo);
			custo += ta - pa;
			ta++;
			pa = strRegua.indexOf('A', pa + 1);
		}
		while (ub >= getNumeroDeBlocos()) {
			// System.out.println("custo="+custo);
			custo += ub - tb;
			tb--;
			ub = strRegua.lastIndexOf('B', ub - 1);
		}

		// System.out.println("<calculaDistanciaTrocas() custo=" + custo);
		return custo;
	}

	private int calculaDistanciaUnitariaTrocas() {
		// System.out.println("<calculaDistanciaTrocas()>");
		int custo = 0;
		int ub = strRegua.lastIndexOf('B');
		int pa = strRegua.indexOf('A');
		int ta = getNumeroDeBlocos();
		int tb = getNumeroDeBlocos();
		while (pa <= getNumeroDeBlocos() && pa >= 0) {

			if (ta - pa > getNumeroDeBlocos())
				custo += 2;
			else if (ta - pa > 0)
				custo++;
			// System.out.println("custoLOOP1=" + custo);
			ta++;
			pa = strRegua.indexOf('A', pa + 1);
		}
		while (ub >= getNumeroDeBlocos()) {
			// custo += ub - tb;
			if (ub - tb > getNumeroDeBlocos())
				custo += 2;
			else if (ub - tb > 0)
				custo++;
			// System.out.println("custoLOOP2=" + custo);
			tb--;
			ub = strRegua.lastIndexOf('B', ub - 1);
		}

		// System.out.println("<calculaDistanciaTrocas() custo=" + custo);
		return custo;
	}

	/**
	 * 
	 * @param numeroDeBlocos
	 * @param numDeReguas
	 * @return
	 */
	public static Vector<Regua> geraReguasAleatoriamente(int numeroDeBlocos,
			int numDeReguas) {
		Random random = new Random(System.currentTimeMillis());
		Vector<Regua> reguas = new Vector<Regua>(numDeReguas);
		char regua[] = new char[numeroDeBlocos * 2 + 1];
		while (reguas.size() < numDeReguas) {
			regua[numeroDeBlocos * 2] = '-';
			int i = 0;
			int numBlocosBrancos = 0;
			for (; i < numeroDeBlocos * 2 && numBlocosBrancos < numeroDeBlocos
					&& i - numBlocosBrancos < numeroDeBlocos; i++) {

				if (random.nextBoolean()) {
					regua[i] = 'B';
					numBlocosBrancos++;
				} else
					regua[i] = 'A';
			}
			char corBlocoFaltante;
			if (numBlocosBrancos < numeroDeBlocos)
				corBlocoFaltante = 'B';
			else
				corBlocoFaltante = 'A';
			while (i < numeroDeBlocos * 2)
				regua[i++] = corBlocoFaltante;
			int posicaoEspacoVazio = (int) ((numeroDeBlocos * 2 + 1) * Math
					.random());
			char swap = regua[posicaoEspacoVazio];
			regua[posicaoEspacoVazio] = '-';
			regua[numeroDeBlocos * 2] = swap;
			Regua novaRegua = new Regua(String.valueOf(regua), numeroDeBlocos);
			if (!novaRegua.isEstadoMeta())
				reguas.add(novaRegua);
		}
		return reguas;
	}

	public int[] getDeslocamentosPossiveis() {
		int min = getMaxDeslocamentoParaEsquerda();
		int max = getMaxDeslocamentoParaDireita();
		int deslocamentos[] = new int[2 * Math.min(-min, max)
				+ Math.abs(max + min)];
		if (min == 0) {
			for (int i = 1; i <= max; i++)
				deslocamentos[i - 1] = i;
		} else if (max == 0) {
			for (int i = -1; i >= min; i--)
				deslocamentos[-i - 1] = i;
		} else if (-min > max) {
			for (int i = 1; i <= max; i++) {
				deslocamentos[2 * i - 2] = -i;
				deslocamentos[2 * i - 1] = i;
			}
			for (int i = -(max + 1); i >= min; i--)
				deslocamentos[max - i - 1] = i;
		} else {
			for (int i = 1; i <= -min; i++) {
				deslocamentos[2 * i - 2] = -i;
				deslocamentos[2 * i - 1] = i;
			}
			for (int i = -min + 1; i <= max; i++)
				deslocamentos[i - min - 1] = i;
		}
		return deslocamentos;
	}
}
