package util;

import java.util.Vector;

import estrategias.EstrategiaDeBusca;

public class BuscadorDaRegua implements Buscador {
	// private static final Buscador instance = new BuscadorDaRegua();

	// public static Buscador getInstance() {
	// return instance;
	// }

	private No meta = null;

	public BuscadorDaRegua() {
	}

	public BuscadorDaRegua(No meta) {
		this.meta = meta;
	}

	public Vector<No> getSucessores(No no) {
		Regua regua = (Regua) no.getObj();
		int[] deslocamentos = regua.getDeslocamentosPossiveis();
		Vector<No> nosGerados = new Vector<No>(deslocamentos.length);
		for (int i = 0; i < deslocamentos.length; i++)
			nosGerados.add(new No(no.getCusto() + Math.abs(deslocamentos[i]),
					Regua.deslocar(regua, deslocamentos[i]), no));

		return nosGerados;
	}

	public boolean isEstadoMeta(No obj) {
		if (meta != null)
			return meta.equals(obj);
		return ((Regua) obj.getObj()).isEstadoMeta();
	}

	public Resposta buscar(No noOrigem, EstrategiaDeBusca estrategia) {
		return estrategia.alcancarMeta(noOrigem, this);
	}
}
