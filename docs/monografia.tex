\documentclass[brazil,11pt]{article}
% Escrevendo em português
\usepackage[utf8]{inputenc} % acentuaçao direta
\usepackage[brazil]{babel}
%\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}           % define uma fonte mais apropriada a ser utilizada 
\usepackage[cyr]{aeguill}          % % To display characters such as as é 
\usepackage{amssymb}               % símbolos matemáticos
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{url}                         
\usepackage{indentfirst}
\usepackage{graphicx}
\usepackage{makeidx}
\usepackage{html}
\usepackage{color}
\usepackage{pseudocode}

\setlength{\topmargin}{-0.200in}
\setlength{\textheight}{9in}
\setlength{\oddsidemargin}{-.100in}
\setlength{\evensidemargin}{-.100in}
\setlength{\textwidth}{7in}

\pagecolor{white}

%espaço entre as linhas
\renewcommand{\baselinestretch}{1.3}

%%%%%%%%%%%%
% Pour inclure des .jpg dans le tex
%\DeclareGraphicsExtensions{.jpg, .eps}
%\DeclareGraphicsRule{.jpg}{eps}{.jpg.bb}{`jpeg2ps -h -r 600` #1}

%configurando PDF, aparece nas propriedades do documento
%\usepackage[pdftex]{color,graphicx}                      %permite o uso de imagens
\usepackage[pdfauthor={Fábio Pisaruk},%     % autor  
            pdftitle={Monografia},%                        % título, ...
            bookmarks,colorlinks]{hyperref}       % marcadores e hyperlinks        

%configurando hyperlinks PDF
\hypersetup{colorlinks,           % configurando hyperlinks para aparecerem em preto...
            citecolor=black,      % ...ficando mais legível na impressão
            filecolor=black,%
            linkcolor=black,%
            urlcolor=black,%
            pdftex}

\makeindex

\title{\Huge{Implementação do algoritmo Breadth-First Heuristic Search} \\
Inteligência Artificial\\}

\author{
aluno: Fábio Pisaruk N.o USP: 3467959 e-mail: pisaruk@gmail.com\\
professor: Leliane Nunes de Barros\\}

\date{IME USP - \today}

\begin{document}
\maketitle
\newpage
\tableofcontents
\newpage
\begin{abstract}
Neste documento apresentaremos o algoritmo BFHS(Breadth-First Heuristic Search), algumas implementações e suas respectivas análises relacionadas ao problema ReguaPuzzle.
\end{abstract} 
\newpage
 \section{Introdução}
\par
\indent
Em muitos problemas de busca, o espaço de estados, por conter muitos elementos, não pode ser mantido completamente em memória.
Algoritmos de busca, como por exemplo: {\textit busca em largura e A*}, mantém em memória duas listas de nós: expandidos e gerados,
limitando, assim, o tamanho das instâncias que podem ser resolvidas, devido ao aumento do número de elementos armazenados em tais listas.
Mostraremos, neste documento, que é possível reduzir, significativamente, o número de elementos armazenados nas listas, viabilizando assim a solução de instâncias maiores para o problema ReguaPuzzle.
Antes de prosseguirmos, o algoritmo aqui descrito é aplicável apenas a grafos não-dirigidos e com arestas de custo unitário.
\par 
\indent
Este documento está organizado da seguinte maneira: 
\begin{itemize}
\item Inicialmente faremos uma breve introdução ao funcionamento do algoritmo tradicional de busca em largura 
\item Apresentaremos as bases teóricas que suportam o algoritmo BFHS
\item Exibiremos duas implementações feitas do algoritmo BFHS
\item Compararemos alguns pontos da implementação
\end{itemize}
\newpage 
\section{Busca em largura}
\par 
\indent
A busca em largura está entre os algoritmos básicos para buscas em grafos com custo unitário nas arestas.
Ela funciona expandindo-se os nós adjacentes ao nó raiz, colocando-os na lista de nós gerados(OpenList) e colocando o 
nó raiz na lista de nós expandidos(ClosedList). 
O processo continua, retirando-se o primeiro nó da OpenList, expandido-o, adicionado-o à ClosedList e 
adicionando os nós gerados ao final da OpenList. 
Cada nó adjacente, antes de ser adicionado à OpenList, guarda o seu nó antecessor, de maneira que
a solução final possa ser gerada a partir do nó destino até o nó raiz. 
O algoritmo pára quando a OpenList estiver vazia ou o nó a ser expandido for o destino.
A seguir exibimos o pseudocódigo: \\
\begin{pseudocode}[ruled]{Busca em Largura}{start,goal}
\PROCEDURE{expandNode}{node,openList,closedList}
	\FOREACH n \in sucessors(node) \DO
	\IF n \notin openList \cup closedList \THEN
	\BEGIN
		openList.addLast(n) \\
		ancestor(n) \GETS node \\
	\END \\
	closedList \GETS closedList \cup \{node\} \\
	openList \GETS openList - \{node\}
\ENDPROCEDURE
\PROCEDURE{constructSolution}{goal}
	\LOCAL{solution \GETS \emptyset,node \GETS goal} \\
	\WHILE node \neq nil \DO
	\BEGIN
	solution.addFirst(node) \\
	node \GETS ancestor(node) 
	\END \\
	\RETURN{solution}	
\ENDPROCEDURE
\MAIN
	\LOCAL{openList \GETS \{start\},closedList \GETS \emptyset} \\
	ancestor(start) \GETS nil \\
	\WHILE openList \neq \emptyset	\DO 
	\BEGIN
		currentNode \GETS firstElement(openList) \\
		\IF \mbox{currentNode is goal} \THEN 
			\RETURN{\CALL{constructSolution}{currentNode}} \\
		\CALL{expandNode}{currentNode,openList,closedList} 
	\END
\ENDMAIN
\end{pseudocode}

O algoritmo de busca em largura consome muita memória por armazenar todos os elementos expandidos e gerados em listas.
Em problemas como o ReguaPuzzle, em que o espaço de estados cresce exponencialmente no comprimento do problema
$(2n+1)\binom{2n}{n}$, a busca em largura não consegue resolver instâncias maiores devido à falta de memória.\\
 
\section{Bases teóricas do BFHS}
\par
\indent
Antes de apresentarmos os fundamentos que suportam o BFHS, analisaremos um pouco mais a fundo algumas 
características do algorimo de busca em largura apresentado anteriormente. \\
Na busca em largura, o primeiro nó da OpenList é expandido, adicionado à ClosedList e seus nós adjacentes 
são inseridos ao final da OpenList.
Devido a maneira em que os nós são expandidos e analisados, podemos criar diversas OpenLists/ClosedLists, 
uma para cada nível: OpenList$_{0}$, ClosedList$_{0}$, OpenList$_{1}$, ClosedList$_{1}$, \ldots
onde OpenList$_{i+1}$ contem os nós gerados no passo i e ClosedList$_{i}$ os nós expandidos no passo i. \\
As listas têm como função impedir que nós repetidos sejam novamente gerados e os ponteiros para os nós antecessores permitem gerar a solução. 
Se quisermos ser capazes de evitar nós repetidos e de construirmos a solução, precisamos dos dois: listas e ponteiros para antecessores. 
Como queremos economizar memória, guardando menos nós nas listas, deixaremos de lado a capacidade de poder gerar a solução final diretamente.
Para grafos não-dirigidos, a fim de evitarmos repetições não precismos armazenar as ClosedList de todos os passos anteriores, basta armazenar aquela referente ao passo anterior, ou seja, durante a iteração i teremos em memória as seguintes listas:
\begin{itemize}
\item OpenList$_{i}$: nós gerados na iteração i-1.
\item OpenList$_{i+1}$: nós gerados na iteração i.
\item ClosedList$_{i-1}$: nós expandidos na iteração i-1.
\item ClosedList$_{i}$: nós expandidos na iteração i.
\end{itemize}
Ao final da iteração i, podemos descartar a ClosedList$_{i-1}$ e a OpenList$_{i}$ estará vazia, pois todos os seus nós 
foram expandidos. \\
O problema da memória parece ter sido amenizado, mas agora precisamos de uma maneira de reconstruir a solução, uma vez
que não podemos simplesmente caminhar pelo ponteiros para os nós antecessores, já que estamos apagando nós que foram 
expandidos nas iterações anteriores. \\
Para isso, armazenaremos um camada intermediária entre o nó raiz e o nó destino, chamada {\textit relay}, ou seja, não apagaremos a ClosedList$_{relay}$. 
A construção da solução se dará de maneira recursiva: uma vez que o destino for alcançado, a solução do problema original(origem $\rightarrow$ destino ) será a concatenação das soluções dos subproblemas: origem $\rightarrow {\textit relay}$, {\textit relay} $\rightarrow$ destino. 
Aqui, {\textit relay} não se refere à ClosedList$_{relay}$, mas sim ao nó antecessor do nó destino. \\
Uma explicação mais detalhada do algoritmo será dada na próxima seção, por enquanto basta ter em mente os dois pontos fundamentais do BFHS:
\begin{itemize}
\item menor quantidade de listas mantidas em memória
\item divisão do problema inicial em subproblemas a fim de encontrar a solução do mesmo.
\end{itemize}
\newpage
\section{Algoritmo BFHS}
Algumas considerações sobre o algoritmo:
\begin{description}
\item[Definição do UpperBound
\footnote{Na implementação original, valores muito altos de UpperBound geram erro no algoritmo.}]
É preciso definir um limite superior para a busca, de maneira que nós sejam ``podados'' durante as gerações dimunindo  
a quantidade de nós nas listas. \\
Caso não seja simples obter um valor para este limite, uma solução alternativa seria executar o BFHS com valores crescentes de limite superior, até que uma solução seja encontrada. \\
Na nossa  implementação, procedemos desta maneira, utilizando o valor inicial do limite superior como sendo o valor da heurística do nó origem.
\item[Heurística]
A fim de diminuir a quantidade de nós gerados, utilizamos uma função heurística.
\end{description}
\newpage
\subsection{Versão original}
\begin{pseudocode}[ruled]{Bread First Heuristic Search}{start,goal,UpperBound}
	\LOCAL{	
		cost(start) \GETS 0,
		ancestor(start) \GETS nil,
		layer \GETS 0,
		relay \GETS \lfloor UpperBound/2 \rfloor
	      } \\
	\GLOBAL{	
		OpenList_{0} \GETS \{start\},
		OpenList_{1} \GETS \emptyset,
		ClosedList_{0} \GETS \emptyset,
		} \\
	\WHILE OpenList_{layer} \cup OpenList_{layer+1} \neq \emptyset \DO
		\WHILE OpenList_{layer} \neq \emptyset \DO
		\BEGIN
			node \GETS \{a | \forall b \in OpenList_{layer}, cost(a) \leq cost(b)\} \\
			OpenList_{layer} \GETS OpenList_{layer} - \{node\} \\
			ClosedList_{layer} \GETS ClosedList_{layer} \cup \{node\} \\
			\IF \mbox{node is goal} \THEN
			\BEGIN
				\IF ancestor(node) = start \THEN \RETURN{\langle start,node  \rangle} \\
			 	noAux \GETS ancestor(node) \\
				\IF 1 < layer \leq relay \OR layer > relay + 1\THEN 
				\CALL{DeletePreviousLayer}{layer,relay,start} \mbox{ (Redefinição dos ponteiros)}  \\
				middle \GETS ancestor(node) \\
				\IF middle = start \THEN 
				\BEGIN	
					middle \GETS noAux \\
					ancestor(node) \GETS middle \\
					ancestor(middle) \GETS start
				\END \\
				\forall x, x \in 	OpenList_{layer} \cup OpenList_{layer+1} \cup 
							ClosedList_{layer} \cup ClosedList_{layer-1}, delete(x) \\
				\IF cost(middle) = 1 \mbox{ (Fim da recursão)} \THEN
					\pi_{0}	\GETS \langle start,middle \rangle 
				\ELSE
					\pi_{0}	\GETS \CALL{BFHS}{start,middle,cost(middle)} \\
				\IF cost(node) - cost(middle) = 1  \mbox{ (Fim da recursão)} \THEN
					\pi_{1} \GETS \langle middle,goal \rangle 
				\ELSE
					\pi_{1}	\GETS \CALL{BFHS}{middle,goal,cost(node) - cost(middle)} \\
				\RETURN{\langle \pi_{0}, \pi_{1} \rangle} \mbox{ (Concatenação das soluções)}
			\END \\
			\CALL{expandNode}{node,UpperBound} \\
		\END \\
		\IF 1 < layer \leq relay \OR layer > relay + 1 \THEN
			\CALL{DeletePreviousLayer}{layer,relay,start} \\
		layer \GETS layer + 1 , OpenList_{layer+1} \GETS \emptyset , ClosedList_{layer} \GETS \emptyset 
\end{pseudocode}
\newpage
\begin{pseudocode}[ruled]{expandNode}{node,UpperBound}
	\FOREACH n \in successors(node) \DO
	\BEGIN
		\IF cost(node) + 1 + heuristic(n) > UpperBound \mbox{ (Poda nós de acordo com a heuristica) }
		\THEN continue \\
		\IF n \in ClosedList_{layer-1} \cup ClosedList_{layer} \mbox{ (Nós repetidos)} 
		\THEN continue \\
		\IF n \in OpenList_{layer} \cup OpenList_{layer+1}  \mbox{ (Nós repetidos)} 
		\THEN continue \\
		cost(n) \GETS  cost(node) + 1 \\
		ancestor(n) \GETS node \\
		OpenList_{layer+1} \GETS OpenList_{layer+1} \cup {n}
	\END
\end{pseudocode}


\begin{pseudocode}[ruled]{deletePreviousLayer}{layer,relay,start}
	\IF layer \leq relay \THEN 
	\BEGIN
		\FOREACH n \in ClosedList_{layer} \DO
			ancestor(n) \GETS start
	\END
	\ELSE
	\BEGIN 
		\FOREACH n \in ClosedList_{layer} \DO
		\BEGIN
			\alpha \GETS ancestor(n) \\
			\WHILE \alpha \notin ClosedList_{relay} \DO
				\alpha \GETS ancestor(\alpha) \\
			ancestor(n) \GETS \alpha
		\END
	\END \\
	\forall n, n \in ClosedList_{layer-1}, delete(n) \\
	ClosedList_{layer-1} \GETS \emptyset
\end{pseudocode}
\newpage
\subsection{Versão utilizando busca em profundidade iterativa nos subproblemas(BFHS-IDS)}
\begin{pseudocode}[ruled]{Bread First Heuristic Search}{start,goal,UpperBound}
	\LOCAL{	
		cost(start) \GETS 0,
		ancestor(start) \GETS nil,
		layer \GETS 0,
		relay \GETS \lfloor UpperBound/2 \rfloor
	      } \\
	\GLOBAL{	
		OpenList_{0} \GETS \{start\},
		OpenList_{1} \GETS \emptyset,
		ClosedList_{0} \GETS \emptyset,
		} \\
	\WHILE OpenList_{layer} \cup OpenList_{layer+1} \neq \emptyset \DO
		\WHILE OpenList_{layer} \neq \emptyset \DO
		\BEGIN
			node \GETS \{a | \forall b \in OpenList_{layer}, cost(a) \leq cost(b)\} \\
			OpenList_{layer} \GETS OpenList_{layer} - \{node\} \\
			ClosedList_{layer} \GETS ClosedList_{layer} \cup \{node\} \\
			\IF \mbox{node is goal} \THEN
			\BEGIN
				\IF ancestor(node) = start \THEN \RETURN{\langle start,node  \rangle} \\
			 	noAux \GETS ancestor(node) \\
				\IF 1 < layer \leq relay \OR layer > relay + 1\THEN 
				\CALL{DeletePreviousLayer}{layer,relay,start} \mbox{ (Redefinição dos ponteiros)} 
				middle \GETS ancestor(node) \\
				\IF middle = start \THEN 
				\BEGIN	
					middle \GETS noAux \\
					ancestor(node) \GETS middle \\
					ancestor(middle) \GETS start
				\END \\
				\forall x, x \in 	OpenList_{layer} \cup OpenList_{layer+1} \cup 
							ClosedList_{layer} \cup ClosedList_{layer-1}, delete(x) \\
				\IF cost(middle) = 1 \mbox{ (Fim da recursão)} \THEN
					\pi_{0}	\GETS \langle start,middle \rangle 
				\ELSE
					\pi_{0}	\GETS \CALL{IDS}{start,middle,depth(middle)-depth(start)} \\
				\IF cost(node) - cost(middle) = 1  \mbox{ (Fim da recursão)} \THEN
					\pi_{1} \GETS \langle middle,goal \rangle 
				\ELSE
					\pi_{1}	\GETS \CALL{IDS}{middle,goal,depth(goal)-depth(middle)} \\
				\RETURN{\langle \pi_{0}, \pi_{1} \rangle} \mbox{ (Concatenação das soluções)}
			\END \\
			\CALL{expandNode}{node,UpperBound} \\
		\END \\
		\IF 1 < layer \leq relay \OR layer > relay + 1 \THEN
			\CALL{DeletePreviousLayer}{layer,relay,start} \\
		layer \GETS layer + 1 , OpenList_{layer+1} \GETS \emptyset , ClosedList_{layer} \GETS \emptyset 
\end{pseudocode}
\newpage
\section{Análise dos algoritmos}
Executamos os algoritmos A*, IDA*, UCS, IDS, DFS, BrFS, BFHS e BFHS-IDS em diversas instâncias do problema 
ReguaPuzzle e comparamos os seguintes aspectos:
\begin{itemize}
\item Quantidade máxima de nós armazenados em memória durante a execução
\item Tempo de execução
\item Fator de ramificação
\item Número de nós visitados
\item Número de nós gerados
\end{itemize}
\subsubsection{Metodologia de geração dos gráficos}
Foram geradas 256 réguas de tamanho 2, 128 de tamanho 3, \dots 8 de tamanho 7 e 4 de tamanho 8.
Para cada tamanho de régua, executamos cada algoritmo de busca a todos os problemas gerados para este tamanho. 
A seguir calculamos as médias dos valores a serem comparados.
\subsubsection{Gráficos}
\includegraphics[width=180mm]{../graficos/memory.eps} 
\includegraphics[width=180mm]{../graficos/tempo.eps} 
\includegraphics[width=180mm]{../graficos/memoryPerGerados.eps} 
\includegraphics[width=180mm]{../graficos/visitados.eps} 
\includegraphics[width=180mm]{../graficos/gerados.eps} 
\includegraphics[width=180mm]{../graficos/fatorramificacao.eps} 
\includegraphics[width=180mm]{../graficos/memoryDivGeradosPerBlocos.eps} 
\includegraphics[width=180mm]{../graficos/compAeIDA.eps} 
\includegraphics[width=180mm]{../graficos/compAeIDAGerados.eps} 
\newpage 
\section{Conclusão} 
Neste documento exibimos o algoritmo BFHS e uma outra versão que utiliza IDS para resolver os subproblemas.
A motivação destes algoritmos é a de permitir a solução de problemas mais complexos, que muitas vezes não podem 
ser obtidas pelos algoritmos tradicionais, devido à grande quantidade de memória emnprega por estes. \\
Concluímos que as duas versões propostas não diferem muito em tempo de execução ou na quantidade máxima de memória 
utilizada. \\
Observamos também que o BFHS é totalmente dependente da heurística empregada, assim como acontece com o IDA* e A*. \\
A quantidade de memória utilizada ainda continua crescendo exponencialmente com o tamanho da régua, apenas mais lentamente
que o A* e IDA*.
\begin {thebibliography}{widest-label}
\bibitem
	RRong Zhou and Eric A. Hansen,
	{\em Breadth First heuristic Search},
	International Conference on Automated Planning and Scheduling,
	2004
\end{thebibliography}
\end{document}
